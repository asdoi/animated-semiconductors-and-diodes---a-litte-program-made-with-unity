﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

private bool oneElectron;
private bool twoElectrons;
public bool AllElectrons;
private Animator Animator;


	// Use this for initialization
	void Start () {
		Animator = GetComponent<Animator>();
		}
	
	// Update is called once per frame
	void Update () {
		
		//AllElectrons
		if (Input.GetKey(KeyCode.W))
		{
			oneElectron = true;
		}
		else
		{
			oneElectron = false;
		}

		if (Input.GetKey(KeyCode.E))
		{
			twoElectrons = true;
		}
		else
		{
			twoElectrons = false;
		}

		if (Input.GetKey(KeyCode.R))
		{
			AllElectrons = true;
		}

		Animator.SetBool("oneElectron", oneElectron);
		Animator.SetBool("twoElectron", twoElectrons);
		Animator.SetBool("AllElectrons", AllElectrons);


	}
}
